# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Final editor: Kenneth Liang
Email: kliang@uoregon.edu

## How to run

-Make sure you have the .ini file in the breverts/ folder
-run ./run.sh under breverts/ directory


